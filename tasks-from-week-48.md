# Tasks starting from week 48

Based on the Git repository I showed you, you should try to implement it yourself, starting from commit [550d09d936309ecefb3ee6e41b73edaec884a8ee](https://gitlab.com/improve-software-quality/js-mocks/commit/550d09d936309ecefb3ee6e41b73edaec884a8ee).

From there you should try to get to the current status of the last public commit. The state that we had a look at together.

Continuing from that, please add a `show()` function to the `Repository` class, but do so using TDD. Write the test first and add the least possible implementation to make the test pass.

Below you'll find notes on implementing the system step by step. You can follow along, or just use them to get unstuck.
The `show()` function isn't mentioned further below as this is outside of the scope of this tutorial and should be done completely by you.

**If you are stuck, please do not hesitate to reach out.**

## DVCS system project

The subject will be a DVCS system that lists repositories and fetches them from a remote source. This source could be GitHub, GitLab or something similar. I will not go into the implementation of the Git\*-APIs. This will be "hidden" behind an interface class.

The goal is to test-drive the creation of a system that has the following functions:

- It lists repositories.
- It caches responses.
- It needs some kind of authorization.

## First step

- Create failing test for fetching repositories

Start with the most simple call to the class.
```js
TypeError: repository.list is not a function

       6 |   describe('.list()', () => {
       7 |     it('should return an array', () => {
    >  8 |       expect(repository.list() instanceof Array).toBe(true);
         |                         ^
       9 |     });
      10 |   });
      11 | });

      at Object.list (test/repository.test.js:8:25)
```
It errors, because the `list()` function isn't written yet.
```js
list() {
  return [];
};
```


Now we want to make sure that repositories are fetched from the remote service (something like GitHub.com). To be able to know whether the remote service wrapper was called, we want to watch for a call to the function. We have to create a mock of the function to watch for calls to it.
```js
it('should fetch the repositories from the remote service', () => {
  repository.list();

  expect(mockFetchRepositories).toHaveBeenCalled();
});
```

This looks rather heavy. There is quite some boilerplate here.
We start with creating a Mock of the function we want to watch `mockFetchRepositories`.
We continue with mocking the whole class `RemoteService` since we do not want to concern ourselves with its implementation at this time. This will be tested in unit tests for that class. We **do have to think about the method that will be called.** So we have to already design the interface of the class, which we intend to use.
```js
const mockFetchRepositories = jest.fn();
jest.mock('../src/RemoteService', () => {
  return jest.fn().mockImplementation(() => {
    return { fetchRepositories: mockFetchRepositories };
  });
});
```

This is the simplest implementation that lets our tests succeed:
```js
import RemoteService from './RemoteService';

export default class Repository {
  list() {
    const service = new RemoteService;
    service.fetchRepositories();
    return [];
  };
}
```

This is obviously of little use. We would like the remote service to return a list of repositories and would expect these repositories to be returned from the function `list()`.
```js
// The list of repositories, we expect to receive,
// wich we pass to the mock implementation as a return value.
const listOfRepos = [
  'repo1',
  'repo2',
  'repo3'
];

// further down inside the beforeEach() function
mockFetchRepositories.mockReturnValue(listOfRepos);
```

```js
it('should return the list of repos from the remote service', () => {
  const result = repository.list();
  expect(result).toEqual(listOfRepos);
});
```

The test fails:

```js
Error: expect(received).toEqual(expected)

Difference:

- Expected
+ Received

- Array [
-   "repo1",
-   "repo2",
-   "repo3",
- ]
+ Array []
```

We change the implementation like this:

```js
list() {
  const service = new RemoteService;
  return service.fetchRepositories();
};
```


